### KPI-отчет для BU
Рассчитывает специфичные метрики (план и факт) для каждого направления BU (Коммерция, SEO, Direct Marketing, Навигация, Context Marketing).  
Сейчас существует как Airflow DAG (параллельные таски для каждой направления + больше DRY) и PowerBI-отчет. 
На основе этого отчета принимаются решения руководителями направлений, расчитываются премии для сотрудников. 


#### Процессинг
* Ходим в клик или вертику, считаем метрики
* Ходим в api сервиса для сбора SEO-метрик
* Процессим данные в pandas'e: меппинг значений, сверка типов, расчет плана (таргета)


