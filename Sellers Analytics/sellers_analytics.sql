--[[Parts of sellers analytics]]
--[NavCategories Tree]
drop table if exists NavCategories;
create local temp table NavCategories on commit preserve rows as (
select distinct n.NavigationalCategoryID as NavCategoryID,
n.Path[0] as NavCategory1ID, n.Path[1] as NavCategory2ID,
n1.Name as NavCategory1, n2.Name as NavCategory2
from (select di.ItemId, ans.SourceKey as SellerId
      from dwh.Dim_Item di
      join dwh_data.Anc_Item ai on ai.ItemId = di.ItemId
      join dwh_data.Anc_Seller ans on ans.SellerId = di.SellerID
      join dwh.StockBalance sb on sb.itemid = ai.SourceKey
      where ans.SourceKey = {seller_id}
      and sb.date >= current_date() and sb.stock >= 1) di
join dwh.Fact_Item_NavigationalCategory aincl on aincl.ItemId = di.itemid
join dwh.Dim_NavigationalCategory n on n.NavigationalCategoryId = aincl.NavigationalCategoryId
join dwh.Dim_NavigationalCategory n1 on n1.NavigationalCategoryID = n.Path[0]
left join dwh.Dim_NavigationalCategory n2 on n2.NavigationalCategoryID = n.Path[1]
) order by NavCategory2ID
segmented by hash(NavCategory2ID) all nodes;
select analyze_statistics('NavCategories');

--[Seller Sales Percentile by YearMonth, NavCat[1-2]]
--Sellers Sales by YearMonth, NavCat[1-2]
drop table if exists SellersSales;
create local temp table SellersSales on commit preserve rows as (
select (date_part('YEAR', a.FactDate)::varchar||'-'||date_part('MONTH', a.FactDate)::varchar) as YearMonth,
i.SellerID, i.NavCategory1, i.NavCategory2,
sum(a.SalesCommercial*a.Qty) as GMV
from (select FactDate, ItemID, SalesCommercial, Qty
      from bi.actions
      where ActionTypeID = {id} and FactDate >= (current_date()-121)) a
join (select distinct di.ItemId, ai.SourceKey as ItemID, ans.SourceKey as SellerID,
      navcat.NavCategory1, navcat.NavCategory2
      from dwh.Dim_Item di
      join dwh_data.Anc_Item ai on ai.ItemId = di.ItemId
      join dwh_data.Anc_Seller ans on ans.SellerId = di.SellerID
      join dwh.Fact_Item_NavigationalCategory aincl on aincl.ItemId = di.ItemId
      join dwh.Dim_NavigationalCategory nc on nc.NavigationalCategoryId = aincl.NavigationalCategoryId
      join NavCategories navcat on navcat.NavCategory2ID = nc.Path[1] and navcat.NavCategory1ID = nc.Path[0]
     ) i on i.ItemID = a.ItemID
group by YearMonth, i.SellerID, i.NavCategory1, i.NavCategory2) order by SellerID
segmented by hash(SellerID) all nodes;
select analyze_statistics('SellersSales');

--Seller GMV_percentile
select *
from (select s.YearMonth, s.SellerID, s.NavCategory1, s.NavCategory1,
percent_rank() over (order by s.GMV asc) as GMV_percentile
from SellersSales s) t
where t.SellerID = 107049;


--[Table DDL]
/*
drop table if exists {team_schema}.SA_TypeMetricsAggregates;
create table if not exists {team_schema}.SA_TypeMetricsAggregates (
    NavCategoryID int not null,
    MedPhotosQty_Type int,
    MedAttributesQty_Type int,
    MedLowAttributesQty_Type int,
    MedMediumAttributesQty_Type int,
    MedHighAttributesQty_Type int,
    MedUnknownAttributesQty_Type int,
    RichContentRate_Type float,
    MedAvgRating_Type float,
    MedReviewsQty_Type int,
    Last30dMedItemPrice_Type float,
    Last30dMedReturnRate_Type float);
*/

--[NavCategories & Items]
--NavCategories Tree
drop table if exists NavCategories;
create local temp table NavCategories on commit preserve rows as (
select distinct n.NavigationalCategoryID as NavCategoryID,
n1.Name as NavCategory1, n2.Name as NavCategory2, n3.Name as NavCategory3,
n4.Name as NavCategory4, n5.Name as NavCategory5, n6.Name as NavCategory6
from dwh.Dim_NavigationalCategory n
join dwh.Dim_NavigationalCategory n1 on n1.NavigationalCategoryID = n.Path[0]
left join dwh.Dim_NavigationalCategory n2 on n2.NavigationalCategoryID = n.Path[1]
left join dwh.Dim_NavigationalCategory n3 on n3.NavigationalCategoryID = n.Path[2]
left join dwh.Dim_NavigationalCategory n4 on n4.NavigationalCategoryID = n.Path[3]
left join dwh.Dim_NavigationalCategory n5 on n5.NavigationalCategoryID = n.Path[4]
left join dwh.Dim_NavigationalCategory n6 on n6.NavigationalCategoryID = n.Path[5]
) order by NavCategoryID
segmented by hash(NavCategoryID) all nodes;
select analyze_statistics('NavCategories');

--NavCategories Stats
drop table if exists NavCategories_Stats;
create local temp table NavCategories_Stats on commit preserve rows as (
select NavCategoryID, GMV, Items, GMV_pct, Items_pct
from (select nv.NavCategoryID,
      sum(a.SalesCommercial*a.Qty) as GMV, count(sd.ItemId) as Items,
      percent_rank() over (order by sum(a.SalesCommercial*a.Qty) asc) as GMV_pct,
      percent_rank() over (order by count(sd.ItemId) asc) as Items_pct
      from dwh.Fact_Item_NavigationalCategory fnv
      join NavCategories nv on nv.NavCategoryID = fnv.NavigationalCategoryId
      join (select ItemId
            from dwh.Fact_StoreBalance_Daily
            where date >= (current_date()-14) and Availability = 1
            group by ItemId
            having sum(Stock-Reserve) > 0) sd on sd.ItemId = fnv.ItemId
      join dwh_data.Anc_Item ai on ai.ItemId = fnv.ItemId
      join bi.actions a on a.ItemID = ai.SourceKey
      where a.ActionTypeID = {id}
      and a.FactDate >= (current_date()-14) and a.FactDate <= (current_date()-1)
      group by nv.NavCategoryID) t
where GMV_pct >= 0.15 and Items_pct >= 0.15) order by NavCategoryID
segmented by hash(NavCategoryID) all nodes;
select analyze_statistics('NavCategories_Stats');

--Items in NavCategories
drop table if exists ItemIDs_Type;
create local temp table ItemIDs_Type on commit preserve rows as (
select aincl.NavigationalCategoryID as NavCategoryID, ns.SourceKey as SiteNavCategoryID,
ai.SourceKey as ItemID, i.ItemID
from dwh.Dim_Item i
join dwh_data.Anc_Item ai on ai.itemid = i.itemid
join dwh.Fact_Item_NavigationalCategory aincl on aincl.ItemId = i.ItemID
join dwh_data.Anc_NavigationalCategory ns on ns.NavigationalCategoryId = aincl.NavigationalCategoryId
where aincl.NavigationalCategoryId in (select distinct NavCategoryID from NavCategories_Stats)
) order by ItemID, NavCategoryID
segmented by hash(ItemID, NavCategoryID) all nodes;
select analyze_statistics('ItemIDs_Type');


--[Metrics]
--Photos
drop table if exists Photos_Type;
create local temp table Photos_Type on commit preserve rows as (
select q.NavCategoryID,
approximate_median(q.PhotosQty_Item) as MedPhotosQty_Type
from (select itit.NavCategoryID, a_val.ItemID as ItemID, count(a_val.ItemAttributeID) as PhotosQty_Item
      from dwh_data.Persisted_Item_ItemVariant_ItemModel_ItemAttributeValue a_val
      join ItemIDs_Type itit on itit.ItemID = a_val.ItemID
	  where a_val.ItemAttributeID in ({attr_id_list})
	  group by itit.NavCategoryID, a_val.ItemID) q
group by q.NavCategoryID) order by NavCategoryID
segmented by hash(NavCategoryID) all nodes;
select analyze_statistics('Photos_Type');

--Attributes
drop table if exists Attributes_Type;
create local temp table Attributes_Type on commit preserve rows as (
select q.NavCategoryID,
approximate_median(q.AttributesQty_Type) as MedAttributesQty_Type
from (select NavCategoryID, ItemID, count(ItemAttributeID) as AttributesQty_Type
      from (select distinct itit.NavCategoryID, a_val.ItemID, a_val.ItemAttributeID
            from dwh_data.Persisted_Item_ItemVariant_ItemModel_ItemAttributeValue a_val
            join ItemIDs_Type itit on itit.ItemID = a_val.ItemID
            where a_val.ItemAttributeID not in ({attr_id_list})) t
      group by NavCategoryID, ItemID) q
group by q.NavCategoryID) order by NavCategoryID
segmented by hash(NavCategoryID) all nodes;
select analyze_statistics('Attributes_Type');

--Priority Attributes
--(temp) atr_cr
drop table if exists atr_cr;
create local temp table atr_cr on commit preserve rows as (
select hash(f.AttributeID) as ItemAttributeID, f.NavCategoryID,
max(case when f.sets = 0 then 0 else (f.Add2Carts/f.Sets*100) end) as cr
from {team_schema}.Fact_Filters f
group by hash(f.AttributeID), f.NavCategoryID) order by ItemAttributeID
segmented by hash(ItemAttributeID) all nodes;
--(temp) atr_rn
drop table if exists atr_rn;
create local temp table atr_rn on commit preserve rows as (
select NavCategoryID, approximate_median(cr) as median_cr
from atr_cr
where cr > 0
group by NavCategoryID) order by NavCategoryID
segmented by hash(NavCategoryID) all nodes;
--(temp) PriorityAttributes
drop table if exists PriorityAttributes;
create local temp table PriorityAttributes on commit preserve rows as (
select ItemAttributeID, atr_cr.NavCategoryID as NavCategoryID, atr_cr.cr as CR,
case when (atr_cr.cr = 0) then 'Low'
     when (atr_cr.cr < ifnull(atr_rn.median_cr, 0)) then 'Medium'
     when (atr_cr.cr >= ifnull(atr_rn.median_cr, 0)) then 'High'
end as Priority
from atr_cr
left join atr_rn on atr_rn.NavCategoryID = atr_cr.NavCategoryID
) order by ItemAttributeID
segmented by hash(ItemAttributeID) all nodes;

--PriorityAttributes_Type
drop table if exists PriorityAttributes_Type;
create local temp table PriorityAttributes_Type on commit preserve rows as (
select q.NavCategoryID,
approximate_median(q.HighAttributesQty_Type) as MedHighAttributesQty_Type,
approximate_median(q.MediumAttributesQty_Type) as MedMediumAttributesQty_Type,
approximate_median(q.LowAttributesQty_Type) as MedLowAttributesQty_Type,
approximate_median(q.UnknownAttributesQty_Type) as MedUnknownAttributesQty_Type
from (select NavCategoryID, ItemID,
	  sum(HighAttribute) as HighAttributesQty_Type,
	  sum(MediumAttributes) as MediumAttributesQty_Type,
	  sum(LowAttributes) as LowAttributesQty_Type,
	  sum(UnknownAttributes) as UnknownAttributesQty_Type
      from (select t.NavCategoryID, t.ItemID, t.ItemID, t.SiteNavCategoryID,
                   case when pa.Priority = 'High' then 1 else 0 end as HighAttribute,
                   case when pa.Priority = 'Medium' then 1 else 0 end as MediumAttributes,
                   case when pa.Priority = 'Low' then 1 else 0 end as LowAttributes,
                   case when pa.Priority is null then 1 else 0 end as UnknownAttributes
            from (select distinct itit.NavCategoryID, itit.ItemID, a_val.ItemID, a_val.ItemAttributeID, itit.SiteNavCategoryID
                  from dwh_data.Persisted_Item_ItemVariant_ItemModel_ItemAttributeValue a_val
                  join ItemIDs_Type itit on itit.ItemID = a_val.ItemID
                  where a_val.ItemAttributeID not in ({attr_id_list})) t
            left join PriorityAttributes pa on pa.ItemAttributeID = t.ItemAttributeID
                                            and pa.NavCategoryID = t.SiteNavCategoryID) t
      group by NavCategoryID, ItemID) q
group by q.NavCategoryID) order by NavCategoryID
segmented by hash(NavCategoryID) all nodes;
select analyze_statistics('PriorityAttributes_Type');
drop table if exists atr_cr;
drop table if exists atr_rn;
drop table if exists PriorityAttributes;

--Rich-content
drop table if exists RichContent_Type;
create local temp table RichContent_Type on commit preserve rows as (
select it.NavCategoryID,
(sum(ifnull(q.isRichContent_Item,0)))/count(it.ItemID) as RichContentRate_Type
from ItemIDs_Type it
left join (select distinct itit.NavCategoryID, itit.ItemID, 1 as isRichContent_Item
	       from ItemIDs_Type itit
	       join dwh_data.Persisted_Item_ItemVariant_ItemModel_ItemAttributeValue a_val on a_val.ItemID = itit.ItemID
	       where a_val.ItemAttributeID in ({attr_id_list})
	      ) q on it.ItemID = q.ItemID
group by it.NavCategoryID) order by NavCategoryID
segmented by hash(NavCategoryID) all nodes;
select analyze_statistics('RichContent_Type');

--Reviews & Rating
drop table if exists ReviewsRating_Type;
create local temp table ReviewsRating_Type on commit preserve rows as (
select q.NavCategoryID,
approximate_median(q.ReviewsQty_Type) as MedReviewsQty_Type,
approximate_median(q.AvgRating_Type) as MedAvgRating_Type
from (select NavCategoryID, ItemID, ReviewsQty_Type, AvgRating_Type
      from ItemIDs_Type i
      join sellercenter_product_service_data.sku_variant sv on sv.sku = i.ItemID
      join (select ifnull(ifnull(sv.parent_id, sv.variant_id), sv.sku) as spu,
            count(r.id) ReviewsQty_Type, avg(r.rating) AvgRating_Type
            from product.reviews r
            join ItemIDs_Type i on i.ItemID = r.item_id
            join seller_service.sku_variant sv on sv.sku = i.ItemID
            where review_type_id = 0 and published_at notnull
            group by ifnull(ifnull(sv.parent_id, sv.variant_id), sv.sku)
           ) spu on spu.spu = ifnull(ifnull(sv.parent_id, sv.variant_id), sv.sku)) q
group by q.NavCategoryID) order by NavCategoryID
segmented by hash(NavCategoryID) all nodes;
select analyze_statistics('ReviewsRating_Type');

--Last 30d Item Price
drop table if exists Price_Type;
create local temp table Price_Type on commit preserve rows as (
select q.NavCategoryID,
approximate_median(MedPrice_Item) as Last30dMedPrice_Type
from (select it.NavCategoryID, it.ItemID,
	  approximate_median(p.DiscountPrice) as MedPrice_Item
	  from ItemIDs_Type it
	  join dwh_data.Fact_MarketingPrice p on p.ItemID = it.ItemId
	                                      and p.StartTime::date >= (current_date()-30)
	  group by it.NavCategoryID, it.ItemID) q
group by q.NavCategoryID) order by NavCategoryID
segmented by hash(NavCategoryID) all nodes;
select analyze_statistics('Price_Type');

--Price Index
drop table if exists PI_Type;
create local temp table PI_Type on commit preserve rows as (
select q.NavCategoryID,
approximate_median(Last30dAvgPI_Item) as Last30dMedPI_Type
from (select it.NavCategoryID, it.ItemID, avg(pi.pi_top_comp) as Last30dAvgPI_Item
      from ItemIDs_Type it
      join pi_team.pi_daily pi on pi.ItemID = it.ItemID
      where pi.Date >= current_date()-30
      group by it.NavCategoryID, it.ItemID) q
group by q.NavCategoryID) order by NavCategoryID
segmented by hash(NavCategoryID) all nodes;
select analyze_statistics('PI_Type');

--Return Rate
drop table if exists ReturnRate_Type;
create local temp table ReturnRate_Type on commit preserve rows as (
select q.NavCategoryID,
approximate_median(q.ReturnRate_Type) as Last30dMedReturnRate_Type
from (select d.NavCategoryID, d.ItemID,
	  case when (d.Orders = 0) then 0 else (d.Returns/d.Orders) end as ReturnRate_Type
	  from (select t.NavCategoryID, t.ItemID,
	        count(distinct t.Returns) as Returns, count(distinct t.Orders) as Orders
	        from (select it.NavCategoryID, it.ItemID,
			      case when (act.ActionTypeID = {id}) then act.ClientOrderID end as Returns,
			      case when (act.ActionTypeID = {id}) then act.ClientOrderID end as Orders
	              from ItemIDs_Type it
	              join bi.actions act on act.ItemID = it.ItemID
	              where act.FactDate >= (current_date()-30) and act.FactDate <= (current_date())
	              and act.ActionTypeID in ({id}, {id})) t
	  group by NavCategoryID, ItemID) d) q
group by q.NavCategoryID) order by NavCategoryID
segmented by hash(NavCategoryID) all nodes;
select analyze_statistics('ReturnRate_Type');


--[All Metrics]
drop table if exists Metrics_Type;
create local temp table Metrics_Type on commit preserve rows as (
select distinct nc.NavCategoryID,
p.MedPhotosQty_Type, a.MedAttributesQty_Type,
pa.MedLowAttributesQty_Type, pa.MedMediumAttributesQty_Type, pa.MedHighAttributesQty_Type, pa.MedUnknownAttributesQty_Type,
rc.RichContentRate_Type,
rr.MedAvgRating_Type, rr.MedReviewsQty_Type,
pp.Last30dMedPrice_Type, pi.Last30dMedPI_Type,
r.Last30dMedReturnRate_Type
from NavCategories_Stats nc
left join Photos_Type p on p.NavCategoryID = nc.NavCategoryID
left join Attributes_Type a on a.NavCategoryID = nc.NavCategoryID
left join PriorityAttributes_Type pa on pa.NavCategoryID = nc.NavCategoryID
left join RichContent_Type rc on rc.NavCategoryID = nc.NavCategoryID
left join ReviewsRating_Type rr on rr.NavCategoryID = nc.NavCategoryID
left join Price_Type pp on pp.NavCategoryID = nc.NavCategoryID
left join PI_Type pi on pi.NavCategoryID = nc.NavCategoryID
left join ReturnRate_Type r on r.NavCategoryID = nc.NavCategoryID
) order by NavCategoryID
segmented by hash(NavCategoryID) all nodes;
select analyze_statistics('Metrics_Type');

--Load to DataMart
drop table if exists {team_schema}.SA_TypeMetricsAggregates;
select * into {team_schema}.SA_TypeMetricsAggregates
from Metrics_Type;

--Type Metrics
select * from {team_schema}.SA_TypeMetricsAggregates
limit 10;
