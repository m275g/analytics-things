--[[Filters sets&unsets&clicks]]
drop temporary table if exists filter_set_unset_users;
create temporary table filter_set_unset_users as select * from (
select date,
       if(action_type = 'set', timestamp, toDateTime(0)) as set_time,
       if(action_type = 'unset', timestamp, toDateTime(0)) as unset_time,
       user_session_id, attributes_platform, object_filter_id, path(page_current_url) as url
from tracker.events
where date = toDate('{date}')
and object_type = 'filter' and action_type in ('set', 'unset')
and attributes_platform in ('ios', 'android', 'site', 'mobile_site')
and attributes_namespace = 'bx'
and page_current in ('search', 'search_suggestions', 'category', 'entry_point_category',
                     'category/brand', 'category/tag', 'category/landing')
group by date, set_time, unset_time,
         user_session_id, attributes_platform, object_filter_id, url);

drop temporary table if exists page_cart_users;
create temporary table page_cart_users as select * from (
select date, timestamp as cart_time,
       user_session_id, attributes_platform, path(page_current_url) as url
from tracker.events e
where date = toDate('{date}')
and action_type = 'to_cart'
and user_session_id global in (select distinct user_session_id from filter_set_unset_users
                               where set_time != 0)
and page_current in ('search', 'search_suggestions', 'category', 'entry_point_category',
                     'category/brand', 'category/tag', 'category/landing')
and attributes_platform in ('ios', 'android', 'site', 'mobile_site')
and attributes_namespace = 'bx'
group by date, cart_time, attributes_platform, user_session_id, url);

drop temporary table if exists product_users;
create temporary table product_users as select * from (
select page_view_id, user_session_id, path(page_current_url) as url
from tracker.events
where date = toDate('{date}')
and user_session_id global in (select distinct user_session_id from filter_set_unset_users
                               where set_time != 0)
and object_type = 'product' and action_type = 'click'
and attributes_platform in ('ios', 'android', 'site', 'mobile_site')
and attributes_namespace = 'bx'
and page_current in ('search', 'search_suggestions', 'category', 'entry_point_category',
                     'category/brand', 'category/tag', 'category/landing'));

drop temporary table if exists pdp_cart_users;
create temporary table pdp_cart_users as select * from (
select e.date,
       e.timestamp as cart_time,
       e.user_session_id, e.attributes_platform, t.url
from tracker.events e
any left join product_users t on e.user_session_id = t.user_session_id
                              and e.previous_page_view_id = t.page_view_id
where date = toDate('{date}')
and action_type = 'to_cart' and page_current = 'pdp'
and attributes_platform in ('ios', 'android', 'site', 'mobile_site')
and attributes_namespace = 'bx'
group by e.date, cart_time, e.user_session_id, e.attributes_platform, t.url);

drop temporary table if exists cart_users;
create temporary table cart_users as select * from (
select * from page_cart_users
union all
select * from pdp_cart_users);

--sets, unsets, carts
select su.date, su.attributes_platform, su.object_filter_id,
       splitByChar('-', splitByChar('/', splitByChar('?', splitByString('category/', su.url)[2])[1])[1])[-1] as category_id,
       countIf(su.set_time, set_time != 0) as sets,
       countIf(su.unset_time, unset_time != 0) as unsets,
       uniqIf(c.user_session_id,
              su.set_time != 0 and c.cart_time != 0 and c.cart_time > su.set_time
              and DateDiff('minute', su.set_time, c.cart_time) < 10
              and (su.unset_time = 0 or (su.set_time <= su.unset_time and su.unset_time < c.cart_time))) as carts
from filter_set_unset_users su
any left join cart_users c on c.user_session_id = su.user_session_id
                           and c.url = su.url
where match(category_id, '^[0-9]+$')
group by date, su.attributes_platform, su.object_filter_id, category_id;
