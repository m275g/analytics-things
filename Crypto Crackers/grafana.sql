--[[Crypto Historical Indicators]]
--[Buy Sell Trades]
select toStartOfInterval(toDateTime(time/1000), interval $interval second) as t, 
if(runningDifference(price) <= 0, 'sell', 'buy') as type,
uniq(trade_id) as trades
from crypto.binancepd_trades
where toDateTime(time/1000) >= toDateTime('$from') 
and toDateTime(time/1000) <= toDateTime('$to')
and is_buyer_maker in ($is_market_maker)
group by t, type 
order by t

--[Buy Sell Diff Price]
select toStartOfInterval(toDateTime(time/1000), interval $interval second) as t, 
avgIf(price, runningDifference(price) > 0) - avgIf(price, runningDifference(price) <= 0) as diff_price
from crypto.binancepd_trades
where toDateTime(time/1000) >= toDateTime('$from') 
and toDateTime(time/1000) <= toDateTime('$to')
and is_buyer_maker in ($is_market_maker)
group by t
order by t

--[OHLC]
select toStartOfInterval(toDateTime(ticks_500_dt/1000), interval $interval second) as t, 
argMin(price, toDateTime(ticks_500_dt/1000)) as open
from crypto.binancews_trade
where toDateTime(ticks_500_dt/1000) >= toDateTime('$from') 
and toDateTime(ticks_500_dt/1000) <= toDateTime('$to')
and is_marker_maker in ($is_market_maker)
group by t
order by t


--[[Crypto Indicators Online (w depth-update)]]
--[Buy-Sell Counts]
select toStartOfInterval(toDateTime(trade_time/1000), interval $interval second) as t, 
countIf(trade_id, type = 'sell') as sell_count,
countIf(trade_id, type = 'buy') as buy_count
from crypto.binancews_trade
where fromUnixTimestamp64Milli(toInt64(event_time)) >= toDateTime('$from')
and fromUnixTimestamp64Milli(toInt64(event_time)) <= toDateTime('$to')
and is_marker_maker in ($is_market_maker)
group by t
order by t

--[Bid vs Ask vs Price]
select toStartOfInterval(toDateTime(event_time/1000), interval $interval second) as t, 
median(bid_first_p) as bid_first_p, median(ask_first_p) as ask_first_p
from crypto.binancews_du
where toDateTime(event_time/1000) >= toDateTime('$from') 
and toDateTime(event_time/1000) <= toDateTime('$to')
group by t
order by t

--[OLHC]
select t, min(open) as open
from (
  select round(trade_id / $ticks) as mt, min(event_time) as t,
  argMin(price,event_time) as open
  from crypto.binancews_trade
  where fromUnixTimestamp64Milli(toInt64(event_time)) >= toDateTime('$from')
  and fromUnixTimestamp64Milli(toInt64(event_time)) <= toDateTime('$to')
  group by mt
  order by t)
group by t
order by t

--[OLHC diff min]
select t,
(min(qty)/min(open_close_diff)) as qty_oc_diff
from (
  select round(trade_id / $ticks) as mt, 
  min(event_time) as t,
  argMax(price,event_time) - argMin(price,event_time) as open_close_diff,
  max(price) - min(price) as min_max_diff,
  sum(quantity) as qty
  from crypto.binancews_trade
  where fromUnixTimestamp64Milli(toInt64(event_time)) >= toDateTime('$from')
  and fromUnixTimestamp64Milli(toInt64(event_time)) <= toDateTime('$to')
  group by mt
  order by t)
group by t
order by t

--[Buy/Sell Qty by Ticks]
select t,
sum(sell_qty_) as sell_qty,
sum(buy_qty_) as buy_qty
from (
  select round(trade_id / $ticks) as mt, 
  min(event_time) as t,
  sumIf(quantity,type = 'sell') as sell_qty_,
  sumIf(quantity,type = 'buy') as buy_qty_
  from $table
  where fromUnixTimestamp64Milli(toInt64(event_time)) >= toDateTime('$from')
  and fromUnixTimestamp64Milli(toInt64(event_time)) <= toDateTime('$to')
  group by mt
  order by t)
group by t
order by t

--[Icebergs]
select t, avg(uniq_trades) as mm_avg_trades_in_orders
from
(select toStartOfInterval(toDateTime(trade_time/1000), interval $interval second) as t, 
buyer_order_id, uniq(trade_id) as uniq_trades
from crypto.binancews_trade
where toDateTime(trade_time/1000) >= toDateTime('$from')
and toDateTime(trade_time/1000) <= toDateTime('$to')
and is_marker_maker = 1
group by buyer_order_id, t
order by t)
group by t
order by t
